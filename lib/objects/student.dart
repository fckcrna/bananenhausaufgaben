class Student {
  Student(this.id, this.nickname);
  int id;
  String nickname;
}

class StudentForTeacher{
  int id;
  String nickname;
  String password;
  int learngroupId;

  StudentForTeacher(
    this.id,
    this.nickname,
    this.password,
    this.learngroupId,
  );

  static StudentForTeacher fromJson(Map map) {
    return StudentForTeacher(
      map["id"],
      map["nickName"],
      map["password"],
      map["group"],
    );
  }
}



