import 'package:fckcrna/objects/student.dart';
import 'package:fckcrna/objects/task.dart';

class Learngroup {
  Learngroup(
    this.id,
    this.name,
    this.teacherId,
    this.teacherName,
    this.tasks,
    this.students,
  );

  final int id;
  final String name;
  final int teacherId;
  final String teacherName;
  final List<Task> tasks;
  final List<StudentForTeacher> students;

  static Learngroup fromJson(Map map) {
    List<dynamic> tasks = map["tasks"];
    List<Task> _tasks = [];
    List<dynamic> students = map["students"];
    List<StudentForTeacher> _students = [];

    tasks.forEach((element) => _tasks.add(Task.fromJson(element)));
    students.forEach((element) => _students.add(StudentForTeacher.fromJson(element)));
    return Learngroup(
      map["id"],
      map["name"],
      map["teacherId"],
      map["teacherName"],
      _tasks,
      _students,
      
    );
  }
}
