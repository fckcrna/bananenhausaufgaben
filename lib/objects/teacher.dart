class Teacher {
  Teacher(
    this.id,
    this.firstName,
    this.lastName,
    this.mail,
  );

  final int id;
  final String firstName;
  final String lastName;
  final String mail;

  static Teacher fromJson (Map map){
    return Teacher(map["id"], map["firstName"], map["lastName"], map["mail"]);
  }

}
