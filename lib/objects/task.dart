class Task {
  Task(
    this.id,
    this.ownerid,
    this.classid,
    this.classname,
    this.title,
    this.text,
    this.subject,
    this.duetime,
    this.createtime,
  );

  int id;
  int ownerid;
  int classid;
  String classname;
  String title;
  String text;
  String subject;
  DateTime duetime;
  DateTime createtime;

  static Task fromJson(Map map) {
    return Task(
      map["id"],
      map["owner"],
      map["learngroup"],
      map["learngroupName"],
      map["title"],
      map["text"],
      map["subject"],
      DateTime.fromMillisecondsSinceEpoch(
        map["dueDate"],
      ),
      DateTime.fromMillisecondsSinceEpoch(
        map["creationDate"],
      ),
    );
  }
}

class StudentTask {
  StudentTask(
    this.id,
    this.task,
    this.studentId,
    this.note,
    this.state,
    this.submissionDate,
  );

  int id;
  Task task;
  int studentId;
  String note;
  int state;
  DateTime submissionDate;

  static StudentTask fromJson(Map map) {
    return StudentTask(
      map["id"],
      Task.fromJson(map["task"]),
      map["student"],
      map["note"],
      map["state"],
      DateTime.fromMillisecondsSinceEpoch(map["submissionDate"] ?? 0),
    );
  }
}

class StudentTaskDetail{
  StudentTaskDetail(
    this.id,
    this.taskId,
    this.studentId,
    this.studentName,
    this.stringNote,
    this.state,
    this.submissionDate,
  );

  int id;
  int taskId;
  int studentId;
  String studentName;
  String stringNote;
  int state;
  DateTime submissionDate;

  static StudentTaskDetail fromJson(Map map) {
    return StudentTaskDetail(
      map["id"],
      map["task"],
      map["student"],
      map["studentName"],
      map["note"],
      map["state"],
      DateTime.fromMillisecondsSinceEpoch(map["submissionDate"] ?? 0),
    );
  }
}