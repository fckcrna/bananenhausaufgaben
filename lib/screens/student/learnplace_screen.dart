import 'package:fckcrna/const.dart';
import 'package:fckcrna/objects/task.dart';
import 'package:fckcrna/widgets/stundent_learnobject_widget.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fckcrna/global.dart' as globals;
import 'dart:convert';

class LernPlatzScreen extends StatefulWidget {
  @override
  _LernPlatzScreenState createState() => _LernPlatzScreenState();
}

class _LernPlatzScreenState extends State<LernPlatzScreen> {
  bool refreshData = true;

  List<StudentTask> todo = [];
  List<StudentTask> finish = [];

  void getData() async {
    var url = basePath + "/learnplace/student?id=" + globals.userid.toString();
    var response = await http.get(url);

    if (response.statusCode == 200) {
      setState(() {
        var json = jsonDecode(response.body);
        List<dynamic> todoD = json["todo"];
        List<dynamic> finishD = json["finished"];
        todoD.forEach((element) => todo.add(StudentTask.fromJson(element)));
        finishD.forEach((element) => finish.add(StudentTask.fromJson(element)));
        print(todo);
        print(finish);
      });
    }
  }

  _LernPlatzScreenState() {
    if (refreshData) {
      getData();
      refreshData = false;
    }
  }

  List<String> aufgaben = [];

  final double i = 6;

  final items = List<String>.generate(6, (i) => "Item $i");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Aufgabenübersicht"),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.language), onPressed: () => {})
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              ExpansionTile(
                title: Text("Aufgaben"),
                subtitle: Text("Hier findest du alles ungelösten Aufgaben"),
                children: <Widget>[
                  SingleChildScrollView(
                    child: Container(
                      height: 300,
                      child: ListView.builder(
                        itemCount: todo.length,
                        itemBuilder: (context, index) {
                          return StudentLearnobjectWidget(
                            todo[index].task,
                            todo[index].state,
                            todo[index].id,
                            "",
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
              ExpansionTile(
                title: Text("Abgeschlossen"),
                subtitle:
                    Text("Hier findest du alles abgeschlossenen Aufgaben"),
                children: <Widget>[
                  Container(
                    height: 300,
                    child: ListView.builder(
                      itemCount: finish.length,
                      itemBuilder: (context, index) {
                        return StudentLearnobjectWidget(
                          finish[index].task,
                          finish[index].state,
                          finish[index].id,
                          finish[index].note
                        );
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ));
  }
}
