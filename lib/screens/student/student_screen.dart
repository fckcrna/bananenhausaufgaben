import 'dart:convert';

import 'package:fckcrna/const.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;
import 'package:fckcrna/global.dart' as globals;

class StudentScreen extends StatefulWidget {
  @override
  _StudentScreenState createState() => _StudentScreenState();
}

class _StudentScreenState extends State<StudentScreen> {
  String nickname;
  String password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Schüler Login"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.language), onPressed: () => {})
        ],
      ),
      body: Container(
        margin: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Als Schüler anmelden",
              style: kLableTextStyle,
            ),
            SizedBox(
              height: 15,
            ),
            TextFormField(
              onChanged: (String string) {
                nickname = string;
              },
              decoration: const InputDecoration(
                icon: const Icon(Icons.people),
                hintText: 'Benutzername',
                labelText: 'Benutzername',
              ),
              keyboardType: TextInputType.text,
            ),
            TextFormField(
              onChanged: (String string) {
                password = string;
              },
              decoration: const InputDecoration(
                icon: const Icon(Icons.lock),
                hintText: 'Passwort',
                labelText: 'Passwort',
              ),
              keyboardType: TextInputType.text,
              obscureText: true,
            ),
            SizedBox(
              height: 10,
            ),
            MaterialButton(
              onPressed: () async {
                print(nickname);
                print(password);
                var bodycontent =
                    "{\"nickname\": \"$nickname\",\"password\": \"$password\"}";
                var url = basePath + "/login/student";
                var response = await http.post(url, body: bodycontent);
                print(response);
                print(response.statusCode);
                print(response.body);

                if (response.statusCode == 200) {
                  setState(() {
                    
                    globals.userid = json.decode(response.body)["id"];
                    print(globals.userid);
                    Toast.show("Erfolgreich angemeldet", context,
                        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                    Navigator.pushReplacementNamed(context, '/lernplatz');
                  });
                } else {
                  Toast.show("Nutername oder Passwort falsch", context,
                      duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                }
              },
              child: Text(
                "Einloggen",
                style: kWhiteTextTextStyle,
              ),
              color: primaryColor,
            ),
            SizedBox(
              height: 50,
            ),
            Container(
              height: 250,
              child: Image.network(
                "https://res.cloudinary.com/devpost/image/fetch/s--eOVqM3cM--/c_limit,f_auto,fl_lossy,q_auto:eco,w_900/https://i.imgur.com/FAL6ggw.png",
                fit: BoxFit.cover,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
