import 'package:fckcrna/const.dart';
import 'package:fckcrna/objects/task.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:convert' show utf8;
import 'package:http/http.dart' as http;

class LearnPlaceDetailScreen extends StatefulWidget {
  final Task task;
  int status;
  final int id;
  bool enable;
  String initNote;

  LearnPlaceDetailScreen({
    this.task,
    this.status,
    this.id,
    this.initNote,
  });

  @override
  _LearnPlaceDetailScreenState createState() => _LearnPlaceDetailScreenState();
}

class _LearnPlaceDetailScreenState extends State<LearnPlaceDetailScreen> {
  String note;

  void statusChangeOnload() async {
    if (widget.status == 0) {
      var url = basePath + "/task/seen?id=" + widget.id.toString();
      var response = await http.post(url);

      if (response.statusCode == 200) {
        widget.status = 4;
      } else {
        this.statusChangeOnload();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    widget.enable = widget.status == 4 || widget.status == 0;
    statusChangeOnload();
    return Scaffold(
      appBar: AppBar(
        title: Text("Aufgabenstellung"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.language), onPressed: () => {})
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Text(
                utf8.decode(widget.task.title.runes.toList()),
                style: kLableTextStyle,
                textAlign: TextAlign.center,
              ),
              Divider(
                endIndent: 10,
                indent: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.category),
                        SizedBox(
                          width: 10,
                        ),
                        Text(widget.task.subject)
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.timer),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          DateFormat('dd.MM.yyyy kk:mm')
                              .format(widget.task.duetime),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Divider(
                endIndent: 10,
                indent: 10,
              ),
              Container(
                margin: EdgeInsets.all(10),
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Aufgabenstellung:",
                    textAlign: TextAlign.left,
                    style: kLabelHeader,
                  )),
              Text(
                widget.task.text,
                style: kAufgabenBeschreibungTextStyle,
              ),
              SizedBox(
                height: 25,
              ),
              // Image.network(
              //     "https://cdn.pixabay.com/photo/2014/05/10/15/29/ice-341324_960_720.jpg"),

              TextFormField(
                enabled: widget.enable,
                decoration: InputDecoration(
                    labelText: "Gib hier deine Lösung an",
                    icon: Icon(Icons.note_add)),
                onChanged: (String string) {
                  note = string;
                },
                initialValue: widget.initNote,
              ),
              SizedBox(
                height: 25,
              ),
              Visibility(
                visible: widget.enable,
                child: MaterialButton(
                  padding: EdgeInsets.all(20),
                  color: primaryColor,
                  onPressed: () async {
                    var bodycontent =
                        "{\"id\": ${widget.id},\"note\": \"$note\"}";
                    var url = basePath + "/task/submit";

                    var response = await http.put(url, body: bodycontent);

                    statusChangeOnload();

                    Navigator.pop(context);
                  },
                  child: Column(
                    children: <Widget>[
                      Icon(
                        Icons.cloud_upload,
                        color: Colors.white,
                      ),
                      Text(
                        "Lösung einreichen",
                        style: kButtonTextTextStyle,
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
