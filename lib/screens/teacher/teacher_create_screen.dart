import 'package:fckcrna/const.dart';
import 'package:flutter/material.dart';

class TeacherCreateScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lehrkraft anlegen"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.language), onPressed: () => {})
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Neue Lehrkraft anlegen",
                style: kLableTextStyle,
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: const Icon(Icons.title),
                  labelText: 'Lehrkraft E-Mail',
                ),
                keyboardType: TextInputType.text,
              ),
              SizedBox(
                height: 10,
              ),
              Divider(),
              SizedBox(
                height: 10,
              ),
              MaterialButton(
                
                padding: EdgeInsets.all(15),
                onPressed: () {},
                child: Text(
                  "Lehrkraft hinzufügen",
                  style: kButtonTextTextStyle,
                ),
                color: Colors.green,
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
