import 'package:fckcrna/const.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fckcrna/global.dart' as globals;
import 'package:toast/toast.dart';


class HomeworkAddScreen extends StatelessWidget {
  final Map<int, String> klassenMap;

  HomeworkAddScreen(
    this.klassenMap,
  );

  String title;
  String text;
  String subject;
  int learngroupid;
  int duedate;
  DateTime time;
  String image;

  DateTime selectedDate = DateTime.now();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime.now().subtract(Duration(days: 1)),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate) {
      selectedDate = picked;
      duedate = selectedDate.millisecondsSinceEpoch;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Neue HA anlegen"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.language), onPressed: () => {})
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Neue Aufgabe anlegen",
                style: kLableTextStyle,
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: const Icon(Icons.title),
                  labelText: 'Hausaufgabentitle',
                ),
                onChanged: (String string) {
                  title = string;
                },
                keyboardType: TextInputType.text,
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: const Icon(Icons.description),
                  //hintText: 'Aufgabenstellung',
                  labelText: 'Aufgabenstellung',
                ),
                onChanged: (String string) {
                  text = string;
                },
                keyboardType: TextInputType.text,
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: const Icon(Icons.category),
                  //hintText: 'Fach',
                  labelText: 'Fach',
                ),
                onChanged: (String string) {
                  
                  subject = string;
                },
                keyboardType: TextInputType.emailAddress,
              ),
              SizedBox(
                height: 10,
              ),
              DropdownButton<int>(
                  items: klassenMap
                      .map((id, name) {
                        return MapEntry(
                          id,
                          DropdownMenuItem<int>(
                            child: Text(name),
                            value: id,
                          ),
                        );
                      })
                      .values
                      .toList(),
                  disabledHint: Text("Klasse auswählen"),
                  hint: Text("Klasse auswählen"),
                  onChanged: (int newValue) {
                    learngroupid = newValue;
                  }),
              MaterialButton(
                minWidth: 250,
                onPressed: () {
                  Toast.show("Diese Funktion kommt in der Zukunft. Sorry :(", context,
                      duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                },
                child: Text(
                  "Foto hinzufügen",
                  style: kButtonTextTextStyle,
                ),
                color: primaryColor,
              ),
              SizedBox(
                height: 10,
              ),
              MaterialButton(
                minWidth: 250,
                onPressed: () {
                  _selectDate(context);

                  print(time);
                },
                child: Text(
                  "Abgabedatum wählen",
                  style: kButtonTextTextStyle,
                ),
                color: primaryColor,
              ),
              SizedBox(
                height: 10,
              ),
              Divider(),
              SizedBox(
                height: 10,
              ),
              MaterialButton(
                padding: EdgeInsets.all(15),
                onPressed: () async {
                  int id = globals.userid;
                  var bodycontent =
                      "{\"teacherId\": $id,\"title\": \"$title\",\"text\": \"$text\",\"subject\": \"$subject\",\"picture\": \"\",\"classId\": $learngroupid,\"duedate\": $duedate}";
                  var url = basePath + "/task/new";
                  var response = await http.post(url, body: bodycontent);

                  print(bodycontent);
                  print(response.body);

                  if (response.statusCode == 200) {
                    Toast.show("Aufgabe wurde hinzugefügt!", context,
                        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);

                    Navigator.pushReplacementNamed(
                        context, '/lernplatzconsole');
                  } else {
                    Toast.show("Das hat leider geklappt", context,
                        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                  }
                  
                },
                child: Text(
                  "Aufgabe erstellen",
                  style: kButtonTextTextStyle,
                ),
                color: Colors.green,
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
