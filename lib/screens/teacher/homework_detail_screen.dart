import 'package:fckcrna/const.dart';
import 'package:fckcrna/objects/task.dart';
import 'package:flutter/material.dart';
import 'dart:convert' show jsonDecode, utf8;
import 'package:http/http.dart' as http;

import 'package:intl/intl.dart';

class HomeworkDetailScreen extends StatefulWidget {
  final Task task;

  HomeworkDetailScreen({
    this.task,
  });

  @override
  _HomeworkDetailScreenState createState() => _HomeworkDetailScreenState(task);
}

class _HomeworkDetailScreenState extends State<HomeworkDetailScreen> {
  List<StudentTaskDetail> studentList = [];

  void getData() async {
    var url = basePath + "/task/details?id=" + task2.id.toString();
    var response = await http.get(url);

    print(response.statusCode);

    if (response.statusCode == 200) {
      setState(() {
        var json = jsonDecode(response.body);
        List<dynamic> students = json['students'];

        students.forEach(
            (element) => studentList.add(StudentTaskDetail.fromJson(element)));

        print(studentList.length);
      });
    }
  }

  void changeState(int id, int index) async {
    var url = basePath + "/task/approve?id=" + id.toString();
    var response = await http.post(url);

    print(url);

    if (response.statusCode == 200) {
      setState(() {
        var json = jsonDecode(response.body);

        studentList.removeAt(index);
        studentList.insert(index, StudentTaskDetail.fromJson(json));
      });
    }

    print(response.statusCode);
    print(response.body);
  }

  Task task2;

  _HomeworkDetailScreenState(
    this.task2,
  ) {
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Aufgabenstellung"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.language), onPressed: () => {})
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Text(
                utf8.decode(widget.task.title.runes.toList()),
                style: kLableTextStyle,
                textAlign: TextAlign.center,
              ),
              Divider(
                endIndent: 10,
                indent: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.category),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          utf8.decode(widget.task.subject.runes.toList()),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.timer),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          DateFormat('dd.MM.yyyy').format(widget.task.duetime),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Divider(
                endIndent: 10,
                indent: 10,
              ),
              Container(
                margin: EdgeInsets.all(10),
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Aufgabenstellung:",
                  textAlign: TextAlign.left,
                  style: kLabelHeader,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                utf8.decode(widget.task.text.runes.toList()),
                style: kAufgabenBeschreibungTextStyle,
              ),
              SizedBox(
                height: 25,
              ),
              // Image.network(
              //     "https://cdn.pixabay.com/photo/2014/05/10/15/29/ice-341324_960_720.jpg"),
              Divider(
                endIndent: 10,
                indent: 10,
              ),
              Container(
                height: MediaQuery.of(context).size.height,
                child: ListView.builder(
                  itemBuilder: (context, i) {
                    String statustext = "Status: ";

                    switch (studentList[i].state) {
                      case 0:
                        statustext += "Erhalten";
                        break;
                      case 1:
                        statustext += "Kontrolle";
                        break;
                      case 2:
                        statustext += "Fertig";
                        break;
                      case 3:
                        statustext += "Nicht Erfolgreich";
                        break;
                      case 4:
                        statustext += "Angesehen";
                        break;
                    }

                    return Card(
                      child: ListTile(
                        title: Text(
                          utf8.decode(
                            studentList[i].studentName.runes.toList(),
                          ),
                        ),
                        subtitle: Text(statustext),
                        trailing: studentList[i].state == 1 ||
                                studentList[i].state == 2
                            ? Checkbox(
                                value: studentList[i].state == 2,
                                onChanged: studentList[i].state == 2
                                    ? null
                                    : (context) {
                                        changeState(studentList[i].id, i);

                                      },
                              )
                            : Text(""),
                      ),
                    );
                  },
                  itemCount: studentList.length,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

/** 
 * 
 * utf8.decode(
                              studentList[i].studentName.runes.toList(),
                            ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: MaterialButton(
                      padding: EdgeInsets.all(10),
                      color: Colors.grey,
                      onPressed: () {
                        Toast.show(
                            "Diese Funktion kommt in der Zukunft. Sorry :(",
                            context,
                            duration: Toast.LENGTH_LONG,
                            gravity: Toast.BOTTOM);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(Icons.cloud_download, color: Colors.white),
                          Text(
                            "Herunterladen",
                            style: kButtonTextTextStyle,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                  ),
                  
                  Expanded(
                    child: MaterialButton(
                      padding: EdgeInsets.all(10),
                      color: Colors.grey,
                      onPressed: () {
                        Toast.show(
                            "Diese Funktion kommt in der Zukunft. Sorry :(",
                            context,
                            duration: Toast.LENGTH_LONG,
                            gravity: Toast.BOTTOM);
                      },
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.cloud_upload,
                            color: Colors.white,
                          ),
                          Text(
                            "Lösung hochladen",
                            style: kButtonTextTextStyle,
                          )
                        ],
                      ),
                    ),
                  ),

                  
                ],
              ),
              */
