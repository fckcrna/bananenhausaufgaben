import 'package:fckcrna/const.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fckcrna/global.dart' as globals;
import 'package:toast/toast.dart';

// TODO: Zurück Button auf die Übersicht zuführne und nicht zum Hauptbildschirm

class ClassCreateScreen extends StatelessWidget {
  String klassenname = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Klasse hinzufügen"),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Neue Klasse anlegen",
                style: kLableTextStyle,
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: const Icon(Icons.title),
                  hintText: 'z.B. 7D',
                  labelText: 'Klassenname',
                ),
                onChanged: (String string) {
                  klassenname = string;
                },
                keyboardType: TextInputType.text,
              ),
              SizedBox(
                height: 10,
              ),
              Divider(),
              SizedBox(
                height: 10,
              ),
              MaterialButton(
                padding: EdgeInsets.all(15),
                onPressed: () async {
                  int id = globals.userid;
                  var bodycontent =
                      "{\"teacherId\": $id,\"name\": \"$klassenname\"}";
                  var url = basePath + "/class/new";
                  var response = await http.post(url, body: bodycontent);

                  if (response.statusCode == 200) {
                    Toast.show("$klassenname wurde hinzugefügt!", context,
                        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                        
                    Navigator.pushReplacementNamed(context, '/lernplatzconsole');
                  } else {
                    Toast.show("Das hat leider geklappt", context,
                        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                  }
                },
                child: Text(
                  "Klasse erstellen",
                  style: kButtonTextTextStyle,
                ),
                color: Colors.green,
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
