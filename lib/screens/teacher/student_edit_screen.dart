import 'dart:convert';
import 'dart:math';

import 'package:fckcrna/const.dart';
import 'package:fckcrna/objects/learngroup.dart';
import 'package:fckcrna/objects/student.dart';
import 'package:fckcrna/widgets/student_card.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class StudentEditScreen extends StatefulWidget {
  final Learngroup learngroup;

  StudentEditScreen(
    this.learngroup,
  );

  @override
  _StudentEditScreenState createState() => _StudentEditScreenState();
}

class _StudentEditScreenState extends State<StudentEditScreen> {
  List<String> nomenListe = [
    "Banane",
    "Hund",
    "Katze",
    "Tiger",
    "Bär",
    "Löwe",
    "Pinguin",
    "Wolf",
    "Vogel",
    "Dino",
    "Oreo",
    "Baum",
    "Android",
    "Apfel",
    "Zaun"
  ];

  List<String> farbenListe = [
    "Gelber",
    "Brauner",
    "Grüner",
    "Blauer",
    "Rote",
    "Lilaner",
    "Weißer",
    "Schwarzer",
    "Grauer",
    "Oranger",
    "Rosa"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Schüler verwalten"),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.language),
              onPressed: () {
                print(
                  farbenListe[Random().nextInt(farbenListe.length)] +
                      nomenListe[Random().nextInt(nomenListe.length)],
                );
              })
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: primaryColor,
        onPressed: () async {
          String namegen = farbenListe[Random().nextInt(farbenListe.length)] +
              nomenListe[Random().nextInt(nomenListe.length)];
          var bodycontent =
              "{\"classId\": ${widget.learngroup.id},\"nickname\": \"$namegen\"}";
          var url = basePath + "/student/new";
          var response = await http.post(url, body: bodycontent);

          if (response.statusCode == 200) {
            setState(() {
              Toast.show("Schüler wurde hinzugefügt!", context,
                  duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
              var json = jsonDecode(response.body);
              widget.learngroup.students.add(StudentForTeacher.fromJson(json));
            });
          } else {
            Toast.show("Es ist ein Fehler aufgetreten", context,
                duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
          }

          // Navigator.pushNamed(context, '/createstudent');
        },
        child: Icon(Icons.add),
      ),
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          Text(
            "Klasse ${widget.learngroup.name}",
            style: kHeadlineTextTextStyle,
          ),
          SizedBox(height: 10),
          Container(
            height: MediaQuery.of(context).size.height - 185,
            child: ListView.builder(
              itemCount: widget.learngroup.students.length,
              itemBuilder: (context, i) {
                return StudentCard(
                  function: () {
                    setState(() {
                      widget.learngroup.students.remove(i);
                    });
                  },
                  student: widget.learngroup.students[i],
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
