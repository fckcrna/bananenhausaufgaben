import 'dart:convert';

import 'package:fckcrna/const.dart';
import 'package:fckcrna/objects/teacher.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class TeacherSignScreen extends StatefulWidget {
  @override
  _TeacherSignScreenState createState() => _TeacherSignScreenState();
}

class _TeacherSignScreenState extends State<TeacherSignScreen> {
  String firstname;

  String lastname;

  String mail;

  String password;

  bool datenschutz = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lehrer Signup"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.language), onPressed: () => {})
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Als Lehrer anmelden",
                style: kLableTextStyle,
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                onChanged: (String string) {
                  firstname = string;
                },
                decoration: const InputDecoration(
                  icon: const Icon(Icons.people),
                  hintText: 'Vorname',
                  labelText: 'Vorname',
                ),
                keyboardType: TextInputType.text,
              ),
              TextFormField(
                onChanged: (String string) {
                  lastname = string;
                },
                decoration: const InputDecoration(
                  icon: const Icon(Icons.people),
                  hintText: 'Nachname',
                  labelText: 'Nachname',
                ),
                keyboardType: TextInputType.text,
              ),
              TextFormField(
                onChanged: (String string) {
                  mail = string;
                },
                decoration: const InputDecoration(
                  icon: const Icon(Icons.mail),
                  hintText: 'E-Mail *',
                  labelText: 'E-Mail *',
                ),
                keyboardType: TextInputType.emailAddress,
              ),
              TextFormField(
                onChanged: (String string) {
                  password = string;
                  if (string.contains("@") == true) {
                  } else {
                    Toast.show("Keine Gültige E-Mail", context,
                        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                  }
                },
                decoration: const InputDecoration(
                  icon: const Icon(Icons.lock),
                  hintText: 'Passwort *',
                  labelText: 'Passwort *',
                ),
                keyboardType: TextInputType.text,
                obscureText: true,
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: <Widget>[
                  Checkbox(
                    value: datenschutz,
                    activeColor: Colors.green,
                    onChanged: (bool newValue) {
                      setState(() {
                        datenschutz = !datenschutz;
                      });
                    },
                  ),
                  Text("Ich akzeptieren die Datenschutzerklärung")
                ],
              ),
              MaterialButton(
                onPressed: () async {
                  if (password == null || mail == null || datenschutz == false) {
                    Toast.show("Bitte Eingabe überprüfen!", context,
                        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                  } else if (mail.contains("@") == true && mail.contains(".")) {
                    var bodycontent =
                        "{\"firstname\": \"$firstname\",\"lastname\": \"$lastname\",\"mail\": \"$mail\",\"password\": \"$password\"}";
                    var url = basePath + "/registration/registerTeacher";
                    var response = await http.post(url, body: bodycontent);
                    //print(response);
                    //print(response.statusCode);
                    //print(response.body);

                    if (response.statusCode == 201) {
                      Toast.show("Erfolgreich registriert!", context,
                          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                      final Map parsed = json.decode(response.body);
                      final teacher = Teacher.fromJson(parsed);
                      print(teacher);
                      Navigator.pop(context);
                    }
                  } else {
                    Toast.show("Bitte gültige E-Mail eingeben!", context,
                        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                  }
                },
                child: Text("Konto erstellen", style: kWhiteTextTextStyle,),
                color: primaryColor,
              ),
              SizedBox(
                height: 20,
              ),
              
            ],
          ),
        ),
      ),
    );
  }
}
