import 'dart:convert';

import 'package:fckcrna/const.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';
import 'package:fckcrna/global.dart' as globals;

class TeacherScreen extends StatefulWidget {
  @override
  _TeacherScreenState createState() => _TeacherScreenState();
}

class _TeacherScreenState extends State<TeacherScreen> {
  String mail;
  String password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Lehrer Login"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.language), onPressed: () => {})
        ],
      ),
      body: Container(
        margin: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Als Lehrer anmelden",
                style: kLableTextStyle,
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                onChanged: (String string) {
                  mail = string;
                },
                decoration: const InputDecoration(
                  icon: const Icon(Icons.people),
                  hintText: 'E-Mail Adresse',
                  labelText: 'E-Mail Adresse',
                ),
                keyboardType: TextInputType.emailAddress,
              ),
              TextFormField(
                onChanged: (String string) {
                  password = string;
                },
                decoration: const InputDecoration(
                  icon: const Icon(Icons.lock),
                  hintText: 'Passwort',
                  labelText: 'Passwort',
                ),
                keyboardType: TextInputType.text,
                obscureText: true,
              ),
              SizedBox(
                height: 10,
              ),
              MaterialButton(
                onPressed: () async {
                  print(mail);
                  print(password);
                  var bodycontent =
                      "{\"mail\": \"$mail\",\"password\": \"$password\"}";
                  var url = basePath + "/login/teacher";
                  var response = await http.post(url, body: bodycontent);
                  print(response);
                  print(response.statusCode);
                  print(response.body);
                  if (response.statusCode == 200) {
                    globals.userid = json.decode(response.body)["id"];
                    print(globals.userid);
                    Toast.show("Erfolgreich angemeldet", context,
                        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                    Navigator.pushReplacementNamed(
                        context, '/lernplatzconsole');
                  } else {
                    Toast.show("E-Mail oder Passwort falsch", context,
                        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                  }
                },
                child: Text(
                  "Einloggen",
                  style: kWhiteTextTextStyle,
                ),
                color: primaryColor,
              ),
              SizedBox(
                height: 20,
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/teachersignup');
                },
                child: Text("Noch kein Konto?"),
              ),
              Container(
                height: 250,
                child: Image.network(
                  "https://res.cloudinary.com/devpost/image/fetch/s--AiwGIbNp--/c_limit,f_auto,fl_lossy,q_auto:eco,w_900/https://i.imgur.com/ahdhLUX.png",
                  fit: BoxFit.cover,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
