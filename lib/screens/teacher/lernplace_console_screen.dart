import 'dart:convert';

import 'package:fckcrna/const.dart';
import 'package:fckcrna/objects/learngroup.dart';
import 'package:fckcrna/objects/task.dart';
import 'package:fckcrna/screens/teacher/homework_add_screen.dart';
import 'package:fckcrna/screens/teacher/student_edit_screen.dart';
import 'package:fckcrna/widgets/teacher_task_object_widget.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fckcrna/global.dart' as globals;

// TODO: Bei keiner Klasse -> Auf dem Screen Bitte Klasse hinzufügen hinzufügen

class LernPlatzConsole extends StatefulWidget {
  @override
  _LernPlatzConsoleState createState() => _LernPlatzConsoleState();
}

class _LernPlatzConsoleState extends State<LernPlatzConsole> {
  final double i = 3;

  final items = List<String>.generate(3, (i) => "Item $i");

  final List<String> myList = ["Deutsch", "Spanisch", "Informatik"];

  final List<String> myListBeschreibung = [
    "AB: S. 195",
    "Referate",
    "Coding Übung",
  ];

  List<Learngroup> learngroups = [];
  List<Task> tasks = [];

  bool refreshData = true;

  void getData() async {
    var url = basePath + "/learnplace/teacher?id=" + globals.userid.toString();
    var response = await http.get(url);

    if (response.statusCode == 200) {
      setState(() {
        var json = jsonDecode(response.body);
        List<dynamic> classes = json["classes"];

        classes.forEach(
            (element) => learngroups.add(Learngroup.fromJson(element)));

        print(tasks.length);
      });
    }
  }

  _LernPlatzConsoleState() {
    if (refreshData) {
      getData();
      refreshData = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Übersicht"),
        actions: <Widget>[
          FlatButton(
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.group,
                  color: Colors.white,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  "Klasse erstellen",
                  style: kWhiteTextTextStyle,
                ),
              ],
            ),
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/createclass');
            },
          )
        ],
      ),
      floatingActionButton: learngroups.isEmpty
          ? Text("")
          : FloatingActionButton(
              backgroundColor: primaryColor,
              onPressed: () {
                setState(() {
                  Map<int, String> klassenMap = {};

                  learngroups.forEach((element) =>
                      klassenMap.putIfAbsent(element.id, () => element.name));

                  print(klassenMap);

                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => HomeworkAddScreen(klassenMap),
                    ),
                  );
                });
              },
              child: Icon(Icons.add),
            ),
      body: learngroups.isEmpty
          ? Center(
              child: Text(
              "Bitte eine Klasse anlegen",
              style: kDomeButtonTextTextStyle,
            ))
          : ListView.builder(
              itemCount: learngroups.length,
              itemBuilder: (context, index) {
                return ExpansionTile(
                  title: Text(learngroups[index].name),
                  subtitle: Text(
                      "Hier sind alle Aufgaben der ${learngroups[index].name}"),
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        MaterialButton(
                          onPressed: () {
                            

                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) =>
                                    StudentEditScreen(learngroups[index])));
                          },
                          child: Text(
                            "Klasse bearbeiten",
                            style: kWhiteTextTextStyle,
                          ),
                          color: primaryColor,
                        ),
                        Text(learngroups[index].tasks.isEmpty
                            ? "Bitte eine neue Aufgabe für diese Klasse anlegen."
                            : ""),
                        SingleChildScrollView(
                          //TODO: Leere Anzahl mit Hinweis
                          child: Container(
                            height: 200,
                            child: ListView.builder(
                              itemCount: learngroups[index].tasks.length,
                              itemBuilder: (context, i) {
                                return TeacherTaskobjectWidget(
                                    learngroups[index].tasks[i]);
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                );
              },
            ),
    );
  }
}
