import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const kLableTextStyle = TextStyle(
  fontSize: 20,
  fontWeight: FontWeight.bold,
  //color: Colors.white,
);

const kFachTextStyle = TextStyle(
  fontSize: 20,
  fontWeight: FontWeight.bold,
  //color: Colors.white,
);

const kLabelHeader = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.bold,
  //color: Colors.white,
);

const kAufgabenBeschreibungTextStyle = TextStyle(
  fontSize: 14,
);

const kButtonTextTextStyle = TextStyle(
  //wieder auf 17 stellen
  fontSize: 15,
  fontWeight: FontWeight.w500,
  color: Colors.white,
);

const kDomeButtonTextTextStyle = TextStyle(
  fontSize: 25,
  fontWeight: FontWeight.bold,
  // color: Colors.white,
);

const kHeadlineTextTextStyle = TextStyle(
  fontSize: 55,
  fontWeight: FontWeight.bold,
  color: Colors.black,
);

const kWhiteTextTextStyle = TextStyle(
  color: Colors.white,
);

const ktitleTextTextStyle = TextStyle(
  fontSize: 25,
  color: Colors.white,
);

const basePath = "http://bananasplit.rocks:8080/bananasplit/v1";

const primaryColor = Color(0xFF93bb50);
const overallBackgroundColor = Color(0xFFFFFDEB);
const c3 = Color(0xFFee7f00);
const c4 = Color(0xFFee7f00);
const c5 = Color(0xFFee7f00);
const c6 = Color(0xFFee7f00);
const c7 = Color(0xFFee7f00);
const c8 = Color(0xFFee7f00);
const c9 = Color(0xFFee7f00);
const c10 = Color(0xFFee7f00);

enum task_status_types { todo, finished }

//TODO Design TODOS:
//Lehrer Login  -  Keine schwarze schrift auf Grün - Das schwarz durch grün ersetzen
//Jede schwarze schrift auf 333/555
//Jede schriftart auf Open sans => Done
