import 'dart:convert';

import 'package:fckcrna/const.dart';
import 'package:fckcrna/objects/task.dart';
import 'package:http/http.dart' as http;
import 'package:fckcrna/global.dart' as globals;

class StudentApi {
  //TODO
  bool loginStudent() {}
  Future<Map<String, List<StudentTask>>> getLearnplaceData() async {
    var url = basePath + "/learnplace/student?id=" + globals.userid.toString();
    var response = await http.get(url);

    if (response.statusCode == 200) {
      Map<String, List<dynamic>> json = jsonDecode(response.body);
      Map<String, List<StudentTask>> result = new Map();
      result.putIfAbsent(
          "todo",
          () => json["todo"]
              .map((element) => StudentTask.fromJson(element))
              .toList());
      result.putIfAbsent(
          "finished",
          () => json["finished"]
              .map((element) => StudentTask.fromJson(element))
              .toList());
      return result;
    } else {
      //TODO Error handling on bad connection status
      return new Map<String, List<StudentTask>>();
    }
  }
}
