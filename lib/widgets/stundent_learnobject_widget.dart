import 'package:fckcrna/const.dart';
import 'package:fckcrna/objects/task.dart';
import 'package:fckcrna/screens/student/learnplace_detail_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:convert' show utf8;

class StudentLearnobjectWidget extends StatelessWidget {
  StudentLearnobjectWidget(
    this._task,
    this.status,
    this.id,
    this.initNote,
  );

  //TODO: Detail Seite Konstaruktor

  final Task _task;

  final int status;
  final int id;
  final String initNote;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Card(
        child: ListTile(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => LearnPlaceDetailScreen(
                  task: _task,
                  status: status,
                  id: id,
                  initNote: initNote,
                ),
              ),
            );
          },
          title: Text(
            utf8.decode(_task.title.runes.toList()),
            style: kLabelHeader,
          ),
          subtitle: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.category,
                color: Colors.grey,
              ),
              Text(
                utf8.decode(_task.subject.runes.toList()),
              ),
              Container(
                width: 5,
              ),
              Icon(Icons.timer, color: Colors.grey),
              Text(
                DateFormat('dd.MM.yyyy kk:mm').format(_task.duetime),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
