import 'dart:convert';

import 'package:fckcrna/const.dart';
import 'package:fckcrna/objects/task.dart';
import 'package:fckcrna/screens/teacher/homework_detail_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TeacherTaskobjectWidget extends StatelessWidget {
  TeacherTaskobjectWidget(
    this._task,

  );

  final Task _task;


  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Card(
        child: ListTile(
          onTap: () {
           Navigator.push(context, MaterialPageRoute(builder: (context) => HomeworkDetailScreen(task: _task,))); 
           // Navigator.pushNamed(context, '/studentdetailpage');
          },
          title: Text(
            utf8.decode(_task.title.runes.toList()),
            style: kLabelHeader,
          ),
          subtitle: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.category,
                color: Colors.grey,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                _task.subject,
              ),
              SizedBox(
                width: 10,
              ),
              Icon(Icons.calendar_today, color: Colors.grey),
              SizedBox(
                width: 10,
              ),
              Text(DateFormat('dd.MM.yyyy').format(_task.duetime))
            ],
          ),
        ),
      ),
    );
  }
}
