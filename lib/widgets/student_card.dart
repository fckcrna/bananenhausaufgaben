import 'package:fckcrna/const.dart';
import 'package:fckcrna/objects/student.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import 'dart:convert' show utf8;

import 'package:http/http.dart' as http;

class StudentCard extends StatefulWidget {
  const StudentCard({this.student, this.function});

  final StudentForTeacher student;
  final Function function;

  @override
  _StudentCardState createState() => _StudentCardState();
}

// TODO: Popup auslösen
// TODO: Teilen Button mit Passwort und Username share

class _StudentCardState extends State<StudentCard> {
  bool delete = false;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: delete == false
          ? ListTile(
              title: Text(
                utf8.decode(widget.student.nickname.runes.toList()),
                style: kLableTextStyle,
              ),
              subtitle: Text(
                widget.student.password == ""
                    ? ""
                    : "Passwort: " +
                        utf8.decode(widget.student.password.runes.toList()),
              ),
              trailing: IconButton(
                icon: Icon(Icons.delete),
                onPressed: () async {
                  var url = basePath +
                      "/student/delete?id=" +
                      widget.student.id.toString();
                  var response = await http.delete(url);

                  if (response.statusCode == 204) {
                    Toast.show("Schüler wurde gelöscht!", context,
                        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                    widget.function();
                    delete = true;
                  } else {
                    Toast.show("Es ist ein Fehler aufgetreten", context,
                        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                  }
                },
              ),
            )
          : Container(
              margin: EdgeInsets.all(20),
              child: Text(
                "Dieser Schüler wurde gelöscht!",
                style: TextStyle(
                    fontSize: 15,
                    fontStyle: FontStyle.italic,
                    color: Colors.grey),
                textAlign: TextAlign.center,
              )),
    );
  }
}
