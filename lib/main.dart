import 'package:fckcrna/const.dart';
import 'package:fckcrna/screens/teacher/add_student_screen.dart';
import 'package:fckcrna/screens/student/learnplace_detail_screen.dart';
import 'package:fckcrna/screens/teacher/class_create_screen.dart';
import 'package:fckcrna/screens/teacher/lernplace_console_screen.dart';
import 'package:fckcrna/screens/student/learnplace_screen.dart';
import 'package:fckcrna/screens/student/student_screen.dart';
import 'package:fckcrna/screens/teacher/teacher_create_screen.dart';
import 'package:fckcrna/screens/teacher/teacher_signup_screen.dart';
import 'package:fckcrna/screens/teacher/teacher_screen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Project Bannanasplit',
      theme: ThemeData(
          appBarTheme: AppBarTheme(
            iconTheme: IconThemeData(color: Colors.white),
            textTheme: TextTheme(title: ktitleTextTextStyle),
          ),
          textTheme: GoogleFonts.openSansTextTheme(
            Theme.of(context).textTheme,
          ),
          primaryColor: primaryColor,
          backgroundColor: overallBackgroundColor),
      routes: {
        '/teacherlogin': (context) => TeacherScreen(),
        '/studentlogin': (context) => StudentScreen(),
        '/teachersignup': (context) => TeacherSignScreen(),
        '/lernplatz': (context) => LernPlatzScreen(),
        '/lernplatzconsole': (context) => LernPlatzConsole(),
        //'/homeworkadd': (context) => HomeworkAddScreen(null),
        '/studentdetailpage': (context) => LearnPlaceDetailScreen(),
        '/createclass': (context) => ClassCreateScreen(),
       // '/studentedit': (context) => StudentEditScreen(),
        '/createstudent': (context) => StudentCreateScreen(),
        '/createteacher': (context) => TeacherCreateScreen(),
      },
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage("https://i.ibb.co/nRDFcPF/bananasplit.png"),
              //"https://images.unsplash.com/photo-1513542789411-b6a5d4f31634?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80"),
              fit: BoxFit.contain,
              alignment: Alignment.bottomCenter),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 100,
            ),
            Text(
              "Ich bin",
              style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/teacherlogin');
                    },
                    child: Card(
                      child: Padding(
                        padding: EdgeInsets.all(20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              FontAwesomeIcons.chalkboardTeacher,
                              size: 40,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Lehrer",
                              style: kLableTextStyle,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, '/studentlogin');
                    },
                    child: Card(
                      child: Padding(
                        padding: EdgeInsets.all(20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              FontAwesomeIcons.userGraduate,
                              size: 40,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Schüler",
                              style: kLableTextStyle,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
